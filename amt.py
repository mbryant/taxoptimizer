#!/usr/bin/env python3

# Authors: Sophia Yao, Matt Bryant
#
# This program attempts to maximize realized income (avoiding taxes) over the
# next N years, particularly around ISOs at a now public company.  The main
# assumption it makes is that share price remains constant of the time period.
#
# NOTE: This was written for the 2020 tax code.  It will almost certainly need
# modifications to work in other years.
# NOTE: This was not written by tax experts, and is not intended to give
# actionable advice.  It may be helpful for getting a feel of your options
# around ISOs.


import argparse
from collections import defaultdict
import math

# Dependencies: pip install tabulate z3-solver
from tabulate import tabulate
from z3 import Optimize, RealVector, Sum, If


###############################################################################
# Arguments
###############################################################################
parser = argparse.ArgumentParser(description='Please read the source code before trusting this program!')

# Required
parser.add_argument('--years', type=int, required=True,
                    help='The number of years to operate on.  This is wildly exponential and may never terminate for n>4.')
parser.add_argument('--base-salary', type=int, required=True,
                    help='Your current base salary.')
# TODO: Handle linear share growth percentages to make this vaguely more realistic
parser.add_argument('--share-price', type=float, required=True,
                    help='The current share price.  This is unfortunately assumed to be invariant over the time period.')

# TODO: We should take a file with grant information instead and handle
# multiple grants.
parser.add_argument('--iso-amount', type=int, required=True, help='The number of ISOs outstanding.')
parser.add_argument('--iso-strike', type=float, required=True, help='The strike price of the ISOs.')

# Optional
parser.add_argument('--money-needed-to-live', type=int, default=50000,
                    help='The amount of money that needs to be taken home in cash each year (to avoid going into debt over future gains).')
parser.add_argument('--salary-adjustment', type=float, default=1.06,
                    help='A guess at annual salary increases (1.06 means a 6%% increase).')
parser.add_argument('--default-returns', type=float, default=1.06,
                    help='A guess at annual market returns (1.06 means a 6%% return).')
parser.add_argument('--amt-credit-discount', type=float, default=0.66,
                    help="AMT credit will be returned eventually, but it may take a long time.  We'll discount the remaining credit and count it as returned in the last year.")
args = parser.parse_args()

###############################################################################
# Constants
###############################################################################
# Federal Income tax
FEDERAL_TAX_BUCKETS = [
    # ($ the bucket ends at, percentage tax on this bucket)
    (9875, 0.10),
    (40125, 0.12),
    (85525, 0.22),
    (163301, 0.24),
    (207350, 0.32),
    (518401, 0.35),
    (999999999999, 0.37)
]
FEDERAL_STANDARD_DEDUCTION = 12400

FEDERAL_LTCG_RATE_LOW = 0.15
FEDERAL_LTCG_RATE_HIGH = 0.20
FEDERAL_LTCG_THRESHOLD = 441450

def federal_ltcg(total_income, longterm_income):
   return If(total_income <= FEDERAL_LTCG_THRESHOLD,
      longterm_income * FEDERAL_LTCG_RATE_LOW,
      (FEDERAL_LTCG_THRESHOLD - total_income + longterm_income) * FEDERAL_LTCG_RATE_LOW + (total_income - FEDERAL_LTCG_THRESHOLD) * FEDERAL_LTCG_RATE_HIGH)

# Federal AMT
FEDERAL_AMT_EXEMPTION = 72900
FEDERAL_AMT_RATE = 0.28
FEDERAL_AMT_PHASEOUT = 518400

# Net Investment Income Tax
NET_INVESTMENT_RATE = 0.038
NET_INVESTMENT_EXEMPTION = 200000

# California State Income Tax
STATE_TAX_BUCKETS = [
    # ($ the bucket ends at, percentage tax on this bucket)
    (8800, 0.01),
    (20883, 0.02),
    (32960, 0.04),
    (45753, 0.06),
    (57824, 0.08),
    (295377, 0.093),
    (354445, 0.103),
    (590742, 0.113),
    (999999999999, 0.123),
]
STATE_STANDARD_DEDUCTION = 4400


###############################################################################
# Z3 Helpers
###############################################################################
def Min(a, b):
    return If(a < b, a, b)

def Max(a, b):
    return If(a > b, a, b)

###############################################################################
# Setup
###############################################################################
solver = Optimize()

salary = [args.base_salary * args.salary_adjustment ** n for n in range(args.years)]

# Exercise and sell in the same year
num_sold = RealVector("num_sold", args.years)
# Exercise and hold for LTCG
num_hold = RealVector("num_hold", args.years)

# Income received in year n from exercising and immediately selling
income_shortterm = RealVector("_income_shortterm", args.years)
# Income received in year n from exercising in year n-1 + selling for LTCG in
# year n
income_longterm = RealVector("_income_longterm", args.years)

###############################################################################
# AMT tax system
###############################################################################
amt_tax_paid = RealVector("amt_tax_paid", args.years)

for n in range(args.years):
    # We want to exercise shares to make profit next year, but under AMT we'll
    # pay that right now and not do anything with it next year.
    bargain_element = income_longterm[n + 1] if n+1 < args.years else 0

    # We also pay tax on LTCG received this year, but since we're assuming the
    # stock price is constant and we already considered these gains under AMT
    # in the previous year (bargain element), we don't need to consider LTCG
    # received in this year.
    income = salary[n] + income_shortterm[n] + bargain_element

    exemption = FEDERAL_AMT_EXEMPTION - If(income >= FEDERAL_AMT_PHASEOUT, (income - FEDERAL_AMT_PHASEOUT) / 4, 0)
    solver.add(amt_tax_paid[n] == (income - Max(exemption, 0)) * FEDERAL_AMT_RATE)

###############################################################################
# Normal tax system
###############################################################################
fed_normal_tax_paid = RealVector("fed_normal_tax_paid", args.years)
state_normal_tax_paid = RealVector("state_normal_tax_paid", args.years)

# For the years when we can exercise
for n in range(args.years):
    def bucket_taxes(name, buckets, considered_income):
        income_bucketed = RealVector("{}_bucketed_income".format(name), len(buckets))

        # Compute the income tax we pay in each bucket
        for (i, (upper_bound, rate)) in enumerate(buckets):
            prev_bound = buckets[i - 1][0] if i > 0 else 0
            income_bucketed[i] = If(considered_income >= upper_bound,
                (upper_bound - prev_bound) * rate,
                Max(considered_income - prev_bound, 0) * rate)

        return income_bucketed

    # Compute the capital gains tax for each bucket.  Capital gains buckets are
    # based on all income.
    non_ltcg_income = salary[n] + income_shortterm[n] - FEDERAL_STANDARD_DEDUCTION
    ltcg_tax_bucketed = If(non_ltcg_income > FEDERAL_LTCG_THRESHOLD,
       FEDERAL_LTCG_RATE_HIGH * income_longterm[n],
       federal_ltcg(non_ltcg_income + income_longterm[n], income_longterm[n]))

    fed_income_bucketed = bucket_taxes("federal", FEDERAL_TAX_BUCKETS,
        salary[n] + income_shortterm[n] - FEDERAL_STANDARD_DEDUCTION)
    solver.add(fed_normal_tax_paid[n] == Sum(fed_income_bucketed) + ltcg_tax_bucketed)

    # Federal taxes ignore LTCG, while California doesn't care.
    state_income_bucketed = bucket_taxes("state", STATE_TAX_BUCKETS,
        salary[n] + income_shortterm[n] + income_longterm[n] - STATE_STANDARD_DEDUCTION)
    solver.add(state_normal_tax_paid[n] == Sum(state_income_bucketed))

###############################################################################
# Tax-system agnostic
###############################################################################
fed_tax_paid = RealVector("fed_tax_paid", args.years)
amt_credit = RealVector("_amt_credit", args.years)
amt_credit_used = RealVector("_amt_credit_used", args.years)
cash = RealVector("_cash", args.years)

# We want to get rid of all options
solver.add(Sum(num_hold) + Sum(num_sold) == args.iso_amount)

# We won't exercise anything new in the last year (one year after we want to be done)
solver.add(num_hold[args.years-1] == 0)
solver.add(num_sold[args.years-1] == 0)

# We can't work with negative quantities
for n in range(args.years):
    solver.add(num_hold[n] >= 0)
    solver.add(num_sold[n] >= 0)
    solver.add(income_shortterm[n] >= 0)
    solver.add(income_longterm[n] >= 0)

# We receive no LTCG in year 0
solver.add(income_longterm[0] == 0)

for n in range(args.years):
    solver.add(income_shortterm[n] == (args.share_price - args.iso_strike) * num_sold[n])
    if n+1 < args.years:
        solver.add(income_longterm[n+1] == (args.share_price - args.iso_strike) * num_hold[n])

    # If we paid more AMT than normal tax, we'll get a credit the next time we don't pay AMT
    solver.add(amt_credit[n] == If(amt_tax_paid[n] > fed_normal_tax_paid[n], amt_tax_paid[n] - fed_normal_tax_paid[n], 0))

    # We'll use our credit this year if we're paying normal tax.
    amt_credit_carryover = Sum(amt_credit[:n]) - Sum(amt_credit_used[:n])
    solver.add(amt_credit_used[n] == If(amt_tax_paid[n] >= fed_normal_tax_paid[n], 0,
                            Min(amt_credit_carryover, fed_normal_tax_paid[n] - amt_tax_paid[n])))

    # Obama Net Investment Income Tax
    if salary[n] > NET_INVESTMENT_EXEMPTION:
        investment_tax = NET_INVESTMENT_RATE * (income_shortterm[n] + income_longterm[n])
    else:
        investment_tax = NET_INVESTMENT_RATE * (income_shortterm[n] + income_longterm[n] + salary[n] - NET_INVESTMENT_EXEMPTION)

    # We pay the max of AMT or normal system, and receive back a credit if we're paying normal tax.
    solver.add(fed_tax_paid[n] == If(amt_tax_paid[n] > fed_normal_tax_paid[n], amt_tax_paid[n], fed_normal_tax_paid[n] - amt_credit_used[n]) + investment_tax)

    # We can't get money back from taxes
    solver.add(fed_tax_paid[n] >= 0)

    # We take home our standard income plus LTCG, minus any state taxes and whatever federal tax we've paid.
    # TODO: Handle state actual tax paid (consider AMT)
    takehome = salary[n] + income_shortterm[n] + income_longterm[n] - fed_tax_paid[n] - state_normal_tax_paid[n]

    # We'll earn interest on this takehome pay.
    solver.add(cash[n] == takehome * (args.default_returns ** (args.years - n)))

    # We don't want to take a loan just to pay taxes.
    solver.add(cash[n] >= args.money_needed_to_live)


# We want to maximize our total cash in pocket.  Additionally, since we'll
# eventually get our AMT credit back, we'll count it as discounted cash.
solver.maximize(Sum(cash) + (Sum(amt_credit) - Sum(amt_credit_used)) * args.amt_credit_discount)

assert str(solver.check()) == "sat"
model = solver.model()

###############################################################################
# Outputs
###############################################################################
def to_int(x):
    return round(x.as_fraction())

# Debug
headers = ["DEBUG VARIABLES"] + ["Year {}".format(n) for n in range(args.years)]
variables = defaultdict(lambda: list(range(args.years)))

for var in model:
    (name, year) = str(var).split("__")
    variables[name][int(year)] = to_int(model[var])

output = [[name] + values for name, values in variables.items()]
print(tabulate(output, headers=headers, tablefmt='simple'))

# Target output
headers = [""] + ["Year {}".format(n) for n in range(args.years)] + ["Total"]
output = [
    ["Exercise and hold"] + [str(to_int(model[x])) for x in num_hold] + [None],
    ["Exercise and sell"] + [str(to_int(model[x])) for x in num_sold] + [None],
    ["Tax paid ($)"] + [str(to_int(model[x])) for x in fed_tax_paid] + [sum([to_int(model[x]) for x in fed_tax_paid])],
    ["Takehome cash ($)"] + [str(to_int(model[x])) for x in cash] + [sum([to_int(model[x]) for x in cash])],
]
print("")
print(tabulate(output, headers=headers, tablefmt='fancy_grid'))
